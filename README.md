# Update Packages

Este Ansible Playbook permite actualizar los paquetes de un servidor RHEL.

Se pueden utilizar las siguientes variables:

- `package_update_packages`: Lista de paquetes a actualizar, por defecto `'*'`
- `package_update_excluded_packages`: Lista de paquetes a ignorar en la actualización, por defecto ninguno.
- `package_update_state`: Estado de los paquetes (`present`, `installed` o `latest`), por defecto `latest`.
